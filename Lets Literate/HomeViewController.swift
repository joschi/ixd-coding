//
//  HomeViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 31.01.22.
//

import UIKit
import AVKit
import AVFoundation
/**
 Controller for the main menu.
 This is the starting point of our app - the first view that is presented to the user.
 The view has four buttons:
 1) AR = shows the video about the (not successfully included) AR experience. This should be deleted in a fully functioning app, of course.
 2) Start Exploration = leads to the exploration view with AR image anchor tracking and the real game experience.
 3) Tutorial = shows the tutorial video that is presented to the user and explains the general functionality of the game.
 4) Reset = resets the stats of the game to get a fresh experience.
 */
class HomeViewController: UIViewController, UIAlertViewDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()
        Game.resetGame()
    }
    /**
     If button AR is clicked play a full screen video.
     */
    @IBAction func btnShowAR(_ sender: Any) {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "ar-mode", ofType: "mp4")!)

        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)

        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player

        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    /**
     If button Reset is clicked reset the game. Function uses the Game.reset function. After that the app presents an alert window to the user that tells him/her that the game was resetted.
     */
    @IBAction func btnResetGame(_ sender: Any) {
        print("Reset Button touched.")
        Game.resetGame()
        let alertController = UIAlertController(title: "Information", message: "Game has been resetted.", preferredStyle: .alert)
            // Initialize Actions
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                print("The user is okay.")
            }
        // Add Actions
        alertController.addAction(okayAction)
        // Present Alert Controller
        self.present(alertController, animated: true, completion: nil)
    }
    /**
     If button Start Exploration is tapped, vibrate and segue to the view of exploration mode --> ExploreViewController.swift.
     */
    @IBAction func btnStartExploration(_ sender: Any) {
        Game.setMode(mode: .exploration)
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        performSegue(withIdentifier: "fromHome2ExploreSegue", sender: self)
    }
    /**
    If button Tutorial is tapped play a full screen video.
     */
    @IBAction func btnPlayTutorialVideo(_ sender: Any) {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "tutorial", ofType: "mp4")!)

        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)

        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player

        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }

}
