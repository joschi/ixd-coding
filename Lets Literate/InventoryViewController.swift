//
//  InventoryViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 02.02.22.
//

import UIKit

/**
 Controller for showing the inventory - depending on the status of the game.
 */
class InventoryViewController: UIViewController {

    @IBOutlet weak var inventoryImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showSolvedImage()
    }
   
    /**
     Checks if at least one station was solved. As we only have one station in the demo this means one station (or more) solved to show the image for a collected inventory. If no station was solved yet show an empty inventory.
     For a fully functional app this should be rewritten to show approriate images for already solved stations.
     */
    private func showSolvedImage() {
        let solved = Game.getSolvedStations()
        if solved.count == 0
        {
            //leeres Bild
            self.inventoryImage.image = UIImage(named: "inventory_not_collected.png")
        } else {
            //zeige Bild für gelöste Password-Station -- ACHTUNG: Demo-Mode, das müsste ausgearbeitet werden bei mehr Stationen
            self.inventoryImage.image = UIImage(named: "inventory_collected2.png")
        }
        
    }

}
