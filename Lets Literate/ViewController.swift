//
//  ViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 31.01.22.
//

import UIKit
import RealityKit

/**
 A left over ... deprecated
 */
class ViewController: UIViewController {
    
    @IBOutlet var arView: ARView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Debug - show info of AR
        arView.debugOptions = [.showFeaturePoints, .showAnchorOrigins, .showAnchorGeometry]
        
        // Load the "Box" scene from the "Experience" Reality File
        //let boxAnchor = try! Letsliterate.loadFirstScene()
        
        // Add the box anchor to the scene
        //arView.scene.anchors.append(boxAnchor)
    }
}
