//
//  ImagesViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 07.02.22.
//

import UIKit
import AVKit
import AVFoundation

/**
 Controller for showing windows in a new view, mostly for
 - the inventory
 - images to show information to the user like tasks that are not implemented in the demo.
 */
class ImagesViewController: UIViewController {

    ///standard image that is shown if no filename is passed through the segue
    var filename = "Avatar.png"
    
    @IBOutlet weak var tasksImages: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tasksImages.image = UIImage(named: filename)
    }

}
