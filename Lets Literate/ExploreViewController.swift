//
//  ExploreViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 02.02.22.
//

import UIKit
import RealityKit
import ARKit
import AVKit
import AVFoundation

/**
 Main controller with almost the whole game logic.
 */
class ExploreViewController: UIViewController, ARSessionDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var rkView: ARView!
    @IBOutlet weak var imageScanMask: UIImageView!
    @IBOutlet weak var labelDecoded: UILabel!
    
    private var _pause : Bool = false
    private var _taskImage : String = "ranking_passwords.png"
    private var _inTask1 : Bool = false
    private var _disclaimer_showed = false
    
    private var subTask = 0
    private var task11_stone_to_use = ""
    private var task12_stones_to_use = ""
    
    
    @IBOutlet weak var btnReflect: UIButton!
    /**
     If button reflect is tapped show image.
     */
    @IBAction func btnReflectAction(_ sender: Any) {
        self._taskImage = "reflect_slide.png"
        performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
    }
    
    @IBOutlet weak var btnUse: UIButton!
    /**
     Within the Brute Force Attack subtask there appears the button Use after an image anchor (stone) was recognized.
     It will show if the right solution was chosen.
     */
    @IBAction func btnUseAction(_ sender: Any) {
        if Game.isInTask1() == true && subTask == 1 {
            print("Task1.1: Use-Button was tapped.")
            if task11_stone_to_use == "1" {
                print("Task1.1: correct stone selected")
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                //self._playPasswordsTaskBruteForceAttackFrom1To3Chars()
                Game.setMode(mode: .task1)
                subTask = 2
                imageScanMask.image = UIImage(named:"bruteForce_mask_scan_four_stones.png")
                self.labelDecoded.isHidden = true
                self.btnUse.isEnabled = false
                self.btnUse.isHidden = true
                rkView.session.run(configurationPasswordsTask2)
                print("Settings: Station: " + Game.getActStation().toString() + ", mode:" + Game.getMode().toString() + ", subTask=" + String(subTask))
            } else {
                print("Task1.1: wrong stone selected")
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                subTask = 1
                self.labelDecoded.text = "This was the wrong answer."
                //self.labelDecoded.isHidden = true
                self.btnUse.isHidden = true
                rkView.session.run(configurationPasswordsTask1)
            }
        }
        if Game.isInTask1() == true && subTask == 2 {
            print("Task1.2: Use-Button was tapped.")
            if task12_stones_to_use == "Hi1?" {
                print("Task1.2: correct stones selected")
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                self._playPasswordsTaskBruteForceAttackOutro()
                subTask = 0
                self.labelDecoded.isHidden = true
                self.btnUse.isHidden = true
                Game.setMode(mode: .station)
                Game.setActStation(station: .passwords)
                imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                print("Changed UI to scan single cube plane")
                Game.getStations()[0].getTasks()[0].setSolved(solved: true)
                rkView.session.run(configurationPasswords)
            } else {
                print("Task1.2: wrong stones selected")
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                subTask = 2
                self.labelDecoded.text = "This was the wrong answer."
                //self.labelDecoded.isHidden = true
                self.btnUse.isHidden = true
                rkView.session.run(configurationPasswordsTask2)
            }
        }
        if Game.isInTask1() == true && subTask == 3 {
            //play video and back to Game mode station
            subTask = 0
            Game.setMode(mode: .station)
            Game.setActStation(station: .passwords)
        }
    }
    
    ///Different configurations for the different modes of the game
    let configurationStations = ARImageTrackingConfiguration() //configuration object for exploration
    let configurationPasswords = ARImageTrackingConfiguration() //configuration object for station passwords - the planes
    let configurationPasswordsTask1 = ARImageTrackingConfiguration() //configuration object for station passwords - the puzzle pieces
    let configurationPasswordsTask2 = ARImageTrackingConfiguration() //configuration object for station passwords - the puzzle pieces
    /**
     After the view is loaded initialize everything for AR experience
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // There must be a set of reference images in project's assets
        guard let referenceImagesStations = ARReferenceImage.referenceImages(inGroupNamed: "AR Exploration", bundle: nil)
                    else { fatalError("Missing expected asset catalog 'AR Exploration' in resources.") }
        guard let referenceImagesPasswords = ARReferenceImage.referenceImages(inGroupNamed: "AR Passwords", bundle: nil)
                    else { fatalError("Missing expected asset catalog 'AR Passwords' in resources.") }
        //For Demo only use four pieces
        guard let referenceImagesPuzzlePieces = ARReferenceImage.referenceImages(inGroupNamed: "AR Stonetextures Demo", bundle: nil)
                    else { fatalError("Missing expected asset catalog 'AR Stonetextures' in resources.") }
        //For real experience use all pieces
        //guard let referenceImagesPuzzlePieces = ARReferenceImage.referenceImages(inGroupNamed: "AR Stonetextures", bundle: nil)
                    //else { fatalError("Missing expected asset catalog 'AR Stonetextures' in resources.") }
        //For Demo only use four pieces
        guard let referenceImagesPuzzlePiecesTogether = ARReferenceImage.referenceImages(inGroupNamed: "AR Complex Passwords", bundle: nil)
                    else { fatalError("Missing expected asset catalog 'AR Stonetextures' in resources.") }
                
        // Set ARView delegate so we can define delegate methods in this controller
        rkView.session.delegate = self

        // Forgot automatic configuration to do it manually instead
        rkView.automaticallyConfigureSession = false

        // Show statistics if desired
        //rkView.debugOptions = [.showStatistics]

        // Disable any unneeded rendering options
        rkView.renderOptions = [.disableCameraGrain, .disableHDR, .disableMotionBlur, .disableDepthOfField, .disableFaceMesh, .disablePersonOcclusion, .disableGroundingShadows, .disableAREnvironmentLighting]

        configurationStations.maximumNumberOfTrackedImages = 1
        configurationStations.trackingImages = referenceImagesStations
        
        configurationPasswords.maximumNumberOfTrackedImages = 1
        configurationPasswords.trackingImages = referenceImagesPasswords
        
        configurationPasswordsTask1.maximumNumberOfTrackedImages = 1
        configurationPasswordsTask1.trackingImages = referenceImagesPuzzlePieces
        
        configurationPasswordsTask2.maximumNumberOfTrackedImages = 1
        configurationPasswordsTask2.trackingImages = referenceImagesPuzzlePiecesTogether
        
        rkView.session.run(configurationStations)
        
        Game.setMode(mode: .exploration)
    }
    /**
     After the view appeared show the disclaimer image.
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear(_:) called")
        if self._disclaimer_showed == false {
            self._taskImage = "explore_disclaimer_02.png"
            performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
            self._disclaimer_showed = true
        }
    }
    
    /*
     self._taskImage = "explore_disclaimer.png"
     performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
     */
    
    /*
     Do something if we found an anchor, depending on the state
     */
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        
        //if the game is in exploration mode, we want to scan start planes of cubes
        if Game.isInExploration() == true {
            guard let imageAnchor = anchors[0] as? ARImageAnchor else {
                print("inExploration: no imageAnchor found")
                return
            }
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_start" {
                print("Start of Station Passwords") //debug
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                self._playIntroStationPasswords()
                //delete the anchor
                rkView.session.pause()
                rkView.session.remove(anchor: imageAnchor)
                print("Removed anchor: anchor_passwords_start") //debug
            
                //new configuration, run session again
                rkView.session.run(configurationPasswords)
                print("Set up new AR configuration: Scan Passwords Cube's sides") //debug
            
                //show new interface
                imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                print("Changed UI to scan single cube plane") //debug
                Game.setMode(mode: .station)
                Game.setActStation(station: .passwords)
                print("Game mode is: " + Game.getMode().toString())
                print("Station is: " + Game.getActStation().toString())
            }
        }
        //We are in station passwords and cube's side task1 was scanned and we are in subtask no 1
        if Game.isInTask1() == true && self.subTask == 1 {
            print("InTask1 == true")
            guard let imageAnchor = anchors[0] as? ARImageAnchor else {
                print("inTask1: no imageAnchor found")
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stone_H_schwarz" { //"demo_stone_texture02" - "stone_H_schwarz"
                print("Station Passwords - Task 1.1: stone for H anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the 'H'."
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                self.task11_stone_to_use = "H"
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stone_i_schwarz" { //"demo_stone_texture03" - "stone_i_schwarz"
                print("Station Passwords - Task 1.1: stone for i anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the 'i'"
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                self.task11_stone_to_use = "i"
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stone_1_schwarz" { //"demo_stone_texture05" - "stone_1_schwarz"
                print("Station Passwords - Task 1.1: stone for One anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the '1'"
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                self.task11_stone_to_use = "1"
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stone_Fragezeichen_schwarz" { //"demo_stone_texture06" - "stone_Fragezeichen_schwarz" //"demo_stone_texture06"
                print("Station Passwords - Task 1.1: stone for questionmark anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the '?'"
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                self.task11_stone_to_use = "?"
                return
            }
        }
        //We are in station passwords and cube's side task1 was scanned and we are in subtask no 2
        if Game.isInTask1() == true && self.subTask == 2 {
            print("InTask1 == true")
            print("subTask == 2")
            guard let imageAnchor = anchors[0] as? ARImageAnchor else {
                print("inTask1: no imageAnchor found")
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stones_correct" {
                print("Station Passwords - Task 1.2: stones_correct anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the 'Hi1?'."
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                btnUse.isEnabled = true
                self.task12_stones_to_use = "Hi1?"
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stones_wrong01" {
                print("Station Passwords - Task 1.2: stones_wrong01 anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the '1i?H'"
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                btnUse.isEnabled = true
                self.task12_stones_to_use = "1i?H"
                return
            }
            if let imageName = imageAnchor.name, imageName  == "stones_wrong02" {
                print("Station Passwords - Task 1.2: stones_wrong01 anchor found")
                rkView.session.remove(anchor: imageAnchor)
                rkView.session.pause()
                self.labelDecoded.text = "This is the '?Hi1'"
                self.labelDecoded.isHidden = false
                self.btnUse.isHidden = false
                btnUse.isEnabled = true
                self.task12_stones_to_use = "1i?H"
                return
            }
            //other possibilities here and up at btn
        }
        //if the game is in station mode, we want to scan task planes of a cube
        if Game.isInStation() == true && Game.getActStation() == .passwords && self._inTask1 == false {
            guard let imageAnchor = anchors[0] as? ARImageAnchor else {
                print("inStation, inPasswords: no imageAnchor found")
                return
            }
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_task3" {
                //delete the anchor
                rkView.session.pause()
                rkView.session.remove(anchor: imageAnchor)
                print("Removed anchor: anchor_passwords_task3")
                
                if Game.getStations()[0].getTasks()[0].isSolved() == false {
                    print("Start of Task 1 of Station Passwords")
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                
                    self._playPasswordsTaskBruteForceAttackIntro()
                    //new configuration, run session again
                    print("Set up new AR configuration: Scan Passwords Cube's Task 1's pieces")
                    rkView.session.run(configurationPasswordsTask1)
                    //show new interface
                    imageScanMask.image = UIImage(named:"bruteForce_mask_scan_single_stone.png") // change to "Scan a Plane to Start a Task"
                    print("Changed UI to scan single stone")
                    Game.setMode(mode: .task1)
                    //logic of task 1
                    self.subTask = 1
                } else {
                    print("Taks 1 was already solved")
                    rkView.session.pause()
                    let alertController = UIAlertController(title: "Information", message: "You've already passed this task. Scan another cube's side to progress.", preferredStyle: .alert)
                        // Initialize Actions
                    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                        }
                    // Add Actions
                    alertController.addAction(okayAction)
                    // Present Alert Controller
                    self.present(alertController, animated: true, completion: nil)
                    imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                    Game.setMode(mode: .station)
                    Game.setActStation(station: .passwords)
                    print("Set up new AR configuration: Scan Passwords Cube's sides")
                    rkView.session.run(configurationPasswords)
                    print("Task 1 done, back to station mode")
                }
            }
            
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_task2" {
                //delete the anchor
                rkView.session.pause()
                rkView.session.remove(anchor: imageAnchor)
                print("Removed anchor: anchor_passwords_task2")
                
                if Game.getStations()[0].getTasks()[1].isSolved() == false {
                    print("Start of Task 2 of Station Passwords")
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    self._taskImage = "ranking_passwords.png"
                    performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
                    print("Show information ... we make just a sleep for 5 seconds")
                    self._pause = true
                    print("all subtasks done ... play a video")
                    Game.getStations()[0].getTasks()[1].setSolved(solved: true)
                    rkView.session.pause()
                    
                } else {
                    print("Taks 2 was already solved")
                    rkView.session.pause()
                    let alertController = UIAlertController(title: "Information", message: "You've already passed this task. Scan another cube's side to progress.", preferredStyle: .alert)
                        // Initialize Actions
                    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                        }
                    // Add Actions
                    alertController.addAction(okayAction)
                    // Present Alert Controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                Game.setMode(mode: .station)
                Game.setActStation(station: .passwords)
                print("Set up new AR configuration: Scan Passwords Cube's sides")
                rkView.session.run(configurationPasswords)
                print("Task 2 done, back to station mode")
            }
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_task1" {
                //delete the anchor
                rkView.session.pause()
                rkView.session.remove(anchor: imageAnchor)
                print("Removed anchor: anchor_passwords_task1")
                
                if Game.getStations()[0].getTasks()[2].isSolved() == false {
                    print("Start of Task 3 of Station Passwords")
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    self._taskImage = "types_of_usage.png"
                    performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
                    print("Show information ... we make just a sleep for 5 seconds")
                    self._pause = true
                    print("all subtasks done ... play a video")
                    Game.getStations()[0].getTasks()[2].setSolved(solved: true)
                    rkView.session.pause()
                    
                } else {
                    print("Taks 3 was already solved")
                    rkView.session.pause()
                    let alertController = UIAlertController(title: "Information", message: "You've already passed this task. Scan another cube's side to progress.", preferredStyle: .alert)
                        // Initialize Actions
                    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                        }
                    // Add Actions
                    alertController.addAction(okayAction)
                    // Present Alert Controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                Game.setMode(mode: .station)
                Game.setActStation(station: .passwords)
                print("Set up new AR configuration: Scan Passwords Cube's sides")
                rkView.session.run(configurationPasswords)
                print("Task 3 done, back to station mode")
            }
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_task4" {
                //delete the anchor
                rkView.session.pause()
                rkView.session.remove(anchor: imageAnchor)
                print("Removed anchor: anchor_passwords_task4")
                
                if Game.getStations()[0].getTasks()[3].isSolved() == false {
                    print("Start of Task 4 of Station Passwords")
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    self._taskImage = "good_bad_02.png"
                    performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
                    print("Show information ... we make just a sleep for 5 seconds")
                    self._pause = true
                    print("all subtasks done ... play a video")
                    Game.getStations()[0].getTasks()[3].setSolved(solved: true)
                    rkView.session.pause()
                    
                } else {
                    print("Taks 4 was already solved")
                    rkView.session.pause()
                    let alertController = UIAlertController(title: "Information", message: "You've already passed this task. Scan another cube's side to progress.", preferredStyle: .alert)
                        // Initialize Actions
                    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                        }
                    // Add Actions
                    alertController.addAction(okayAction)
                    // Present Alert Controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                Game.setMode(mode: .station)
                Game.setActStation(station: .passwords)
                print("Set up new AR configuration: Scan Passwords Cube's sides")
                rkView.session.run(configurationPasswords)
                print("Task 4 done, back to station mode")
            }
            //Final Task 5
            //Todo: Erst auslösen, wenn alle anderen Tasks absolviert wurden
            if let imageName = imageAnchor.name, imageName  == "anchor_passwords_end" {
                print("Solved tasks: " + String(Game.getStations()[0].getSolvedTasks().count))
                if (Game.getStations()[0].getSolvedTasks().count < 4)
                {
                    rkView.session.pause()
                    let alertController = UIAlertController(title: "Information", message: "You have to solve the other 4 tasks first before you can unlock this final task.", preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                        }
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                    rkView.session.remove(anchor: imageAnchor)
                    print("Removed anchor: anchor_passwords_end")
                    imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                    Game.setMode(mode: .station)
                    print("Set up new AR configuration: Scan Passwords Cube's sides")
                    rkView.session.run(configurationPasswords)
                    print("Task 5 cannot be done yet, first other 4 Tasks must be solved.")
                } else {
                    //delete the anchor
                    rkView.session.pause()
                    rkView.session.remove(anchor: imageAnchor)
                    print("Removed anchor: anchor_passwords_end")
                
                    if Game.getStations()[0].getTasks()[4].isSolved() == false {
                        print("Start of final Task 5 of Station Passwords")
                        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                        self._taskImage = "strong_password.png"
                        performSegue(withIdentifier: "fromExplore2ImagesSegue", sender: self)
                        print("Show information ... we make just a sleep for 5 seconds")
                        self._pause = true
                        print("all subtasks done ... play a video")
                        Game.getStations()[0].getTasks()[4].setSolved(solved: true)
                        Game.getStations()[0].setSolved(solved: true)
                        //collect
                        rkView.session.pause()
                        imageScanMask.image = UIImage(named:"explore_mask_scan.png")
                        Game.setMode(mode: .station)
                        print("Set up new AR configuration: Find a Cube")
                        rkView.session.run(configurationPasswords)
                        print("Final task 5 done, back to explore mode")
                        let timer2 = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) {
                            timer in
                                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                                self._playPasswordsOutro()
                                self.btnReflect.isHidden = false
                        }
                        Game.setMode(mode: .exploration)
                        Game.setActStation(station: .none)
                        self.imageScanMask.isHidden = true
                    } else { // this should never happen
                        print("Task 5 was already solved")
                        rkView.session.pause()
                        let alertController = UIAlertController(title: "Information", message: "You've already passed this task and the whole station. Scan another cube to progress.", preferredStyle: .alert)
                        // Initialize Actions
                        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) -> Void in
                            print("The user is okay.")
                            }
                        // Add Actions
                        alertController.addAction(okayAction)
                        // Present Alert Controller
                        self.present(alertController, animated: true, completion: nil)
                        
                        imageScanMask.image = UIImage(named:"station_passwords_scan_cube_plane.png")
                        Game.setMode(mode: .station)
                        print("Set up new AR configuration: Scan Passwords Cube's sides")
                        rkView.session.run(configurationPasswords)
                        print("Task 5 done, back to station mode")
                    }
                    
                }
            }
        }
    }
    
    private func _playIntroStationPasswords() {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "passwordsIntro", ofType: "mp4")!)

        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)

        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player

        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    private func _playPasswordsTaskBruteForceAttackIntro() {
        print("Play Intro Video for Brute Force Attack")
        guard let filePath = Bundle.main.path(forResource: "passwordsBruteForceIntro", ofType: "m4v")
            else {
                print("Video passwordsBruteForceIntro.m4v not found.")
                return
            }
        let player = AVPlayer(url: URL(fileURLWithPath: filePath))
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true)
        return
    }
    
    private func _playPasswordsTaskBruteForceAttackFrom1To3Chars() {
        guard let filePath = Bundle.main.path(forResource: "passwordsBruteForceFrom1To3", ofType: "mp4")
            else {
                print("Video passwordsBruteForceFrom1To3.mp4 not found.")
                return
            }
        let player = AVPlayer(url: URL(fileURLWithPath: filePath))
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true)
    }
    
    private func _playPasswordsTaskBruteForceAttackOutro() {
        guard let filePath = Bundle.main.path(forResource: "passwordsBruteForceOutro", ofType: "mp4")
            else {
                print("Video passwordsBruteForceOutro.mp4 not found.")
                return
            }
        let player = AVPlayer(url: URL(fileURLWithPath: filePath))
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true)
    }
    
    private func _playPasswordsOutro() {
        guard let filePath = Bundle.main.path(forResource: "passwordsOutro", ofType: "mp4")
            else {
                print("Video passwordsOutro.mp4 not found.")
                return
            }
        let player = AVPlayer(url: URL(fileURLWithPath: filePath))
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true)
    }
        
    
    @IBAction func btnOpenInventory(_ sender: Any) {
        performSegue(withIdentifier: "fromExplore2InventorySegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromExplore2ImagesSegue" {
            print("We do the segue to tasks' images.")
            let controller = segue.destination as! ImagesViewController
            controller.filename = self._taskImage
        }
    }
    @IBAction func btnMenu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
