//
//  Task.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 05.02.22.
//

import Foundation
/**
 Internal representation of a task at a station.
 */
class Task {
    /// name of the task
    private var name : String = ""
    /// description of the task
    private var description : String = ""
    /// true, if tasked has been solved; otherwise false. Initialized as not solved.
    private var solved : Bool = false
    /// true, if this is the final task on a station; otherwise false. Initialized as false. If final task then the task should not be reachable until the other station's task has been solved.
    private var isFinal : Bool = false
    
    /**
     Constructor
     Sets name to standard NO_TASKNAME
     Sets description to standard NO_DESCRIPTION
     Sets solved to false
     */
    init() {
        self.name = "NO_TASKNAME"
        self.description = "NO_DESCRIPTION"
        self.solved = false
    }
    /**
     Set a new name.
     - Parameter name: new name for the task
     */
    public func setName (name : String) {
        if name.count > 0 {
            self.name = name
        }
    }
    /**
     Returns the actual value of the name.
     -  Returns: name of the task
     */
    public func getName() -> String {
        return self.name
    }
    /**
     Set a new description.
     - Parameter description: the new description for the task.
     */
    public func setDescription (description : String) {
        if description.count > 0 {
            self.description = description
        }
    }
    /**
     Returns the description of the task.
     - Returns: description of the task
     */
    public func getDescription() -> String {
        return self.description
    }
    /**
     Sets the new value for the task as has been solved (true) or not (false).
     - Parameter solved: true, if task has been solved; false otherwise.
     */
    public func setSolved (solved : Bool) {
        self.solved = solved
    }
    /**
     Returns, if the task has been solved already.
     - Returns: True, if task has been solved; false otherwise.
     */
    public func isSolved () -> Bool {
        return self.solved
    }
    /**
     Sets the task to be the final one (true) or not (false).
     - Parameter isFinal: true, if this is the final task; false otherwise.
     */
    public func setFinal (isFinal : Bool) {
        self.isFinal = isFinal
    }
    /**
     Returns, if this is the final task of the station. (isFinal already reserved by Swift, so other function name was needed).
     - Returns: true, if final task; false otherwise.
     */
    public func getIsFinal () -> Bool {
        return self.isFinal
    }
}
