//
//  VideoTutorialViewController.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 31.01.22.
//

import UIKit
import AVKit
import AVFoundation
/**
 Deprecated class, never used.
 */
class VideoTutorialViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnPlayVideo(_ sender: Any) {
        guard let url = URL(string: "https://example.com/my-example-video.m3u8") else { return }

        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)

        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player

        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
