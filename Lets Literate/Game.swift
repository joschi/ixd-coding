//
//  Game.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 02.02.22.
//

import Foundation
/**
 Enum holds the different modes in which the game can be. Add more if needed.
 */
enum modes {
    case exploration, station, menu, inventory, task, task1
    
    func toString() -> String {
        switch self {
        case .exploration:
            return  "exploration"
        case .station:
            return  "station"
        case .menu:
            return  "menu"
        case .inventory:
            return  "inventory"
        case .task:
            return  "task"
        case .task1:
            return "task1"
        }
     }
}
/**
 Enum holds the different stations. It also holds representations of the stations as strings for printing out or debugging.
 Add more station in here and afterwards implement them at the end of the file, see example passwords and socialmedia.
 */
enum gamestations {
    case none, passwords, socialmedia, wellbeing
    
    func toString() -> String {
        switch self {
        case .none:
            return  "none"
        case .passwords:
            return  "passwords"
        case .socialmedia:
            return  "socialmedia"
        case .wellbeing:
            return  "wellbeing"
        }
     }
}
/**
 This class holds all the functionality of the interal representation of the game, and all the functionality to set and get it.
 So most of the attributes and methods are static, because there is only one game. Different instances of the game make no sense.
 */
class Game {
    /// internal representation of the mode of the game. The game starts in menu, so the variable is initialized with this value.
    private static var mode : modes = .menu
    /**
     Sets the actual mode of the game.
     - Parameter mode: New mode in which the game is in. Possible values come from the enum modes.
     */
    public static func setMode (mode:modes) {
        Game.mode = mode
    }
    /**
     Returns the actual mode of the game.
     - Returns: Mode in which the game is. Modes are in the enum modes.
     */
    public static func getMode () -> modes {
        return Game.mode
    }
    /**
     Returns if the game is in mode menu.
     - Returns: True if game mode is menu, otherwise false.
     */
    public static func isInMenu () -> Bool {
        if Game.mode == .menu {
            return true
        }
        return false
    }
    /**
     Returns if the game is in mode exploration.
     - Returns: True if game mode is exploration, otherwise false.
     */
    public static func isInExploration () -> Bool {
        if Game.mode == .exploration {
            return true
        }
        return false
    }
    /**
     Returns if the game is in mode station.
     - Returns: True if game mode is station, otherwise false.
     */
    public static func isInStation () -> Bool {
        if Game.mode == .station {
            return true
        }
        return false
    }
    /**
     Returns if the game is in mode inventory.
     - Returns: True if game mode is inventory, otherwise false.
     */
    public static func isInInventory () -> Bool {
        if Game.mode == .inventory {
            return true
        }
        return false
    }
    /**
     Returns if the game is in mode task.
     - Returns: True if game mode is task, otherwise false.
     */
    public static func isInTask () -> Bool {
        if Game.mode == .task {
            return true
        }
        return false
    }
    /**
     Returns if the game is in mode task1.
     - Returns: True if game mode is task1, otherwise false.
     */
    public static func isInTask1 () -> Bool {
        if Game.mode == .task1 {
            return true
        }
        return false
    }
    /// The array of all stations that were set up and added
    private static var stations : [Station] = []
    /**
     Adds the given station to the internal array of stations.
     - Parameter station: The station that should be added. The station should be set up correctly before.
     */
    private static func addStation (station:Station) {
        Game.stations.append(station)
    }
    /**
     Returns a specific station at a position in the internal array of stations.
     IMPORTANT: No check is done if the position is out of bounds of the array. Be careful.
     - Returns: The station at the position of the internal array
     */
    private static func getStation (position:Int) -> Station {
        return Game.stations[position]
    }
    /**
     Function returns a station with the given name or nil if no station with the name exists.
     - Parameter name: Name of the searched/specific station
     - Returns: Station with the given name or nil if no such station exists.
     */
    private static func getStation (name:String) -> Station? {
        for station in Game.stations {
            if station.getName() == name {
                return station
            }
        }
        return nil
    }
    
    /**
     Removes all stations from the internal array. Function is needed to do a full reset of the game.
     */
    private static func removeStations () {
        Game.stations.removeAll()
    }
    
    /**
     Returns all stations that where set-up in the initialization of the game.
     - Returns: All stations as an array of objects of type Station.
     */
    public static func getStations () -> [Station] {
        return Game.stations
    }
    
    /**
     Returns all stations that are solved. So the inventory can show the collectables already achieved.
     - Returns: Returns all solved stations as an array of objects of type Station.
     */
    public static func getSolvedStations () -> [Station] {
        var solved : [Station] = []
        for station in Game.stations {
            if station.isSolved() == true {
                solved.append(station)
            }
        }
        return solved
    }
    
    /// The actual station where the game is in. Initialized with gamestations.none.
    private static var station : gamestations = .none
    
    /**
    Sets the actual station.
     - Parameter station: The actual station. Must be an entry of enum gamestations.
     */
    public static func setActStation (station : gamestations) {
        self.station = station
    }
    
    /**
     Returns the actual station where the game is.
     - Returns: The actual station where the game is.
     */
    public static func getActStation () -> gamestations {
        return self.station
    }
    
    /**
     Resets all game variables to start again.
     Function should be called at the start of the application and when a button for resetting is pressed.
     In all other cases the pupils can go resume the game from the main menu with the opened stations.
     */
    public static func resetGame () {
        print("Game mode is set to menu.")
        //we are in the main menu
        Game.setMode(mode: .menu)
        
        print("Actual station is set to none.")
        Game.setActStation(station: .none)
        Game.removeStations()
        
        //initialize the password station
        print("Initialize Station Passwords")
        let passwords = Game._prepareStationPasswords()
        print("Initialize Station Social Media")
        let socialmedia = Game._prepareStationSocialmedia()
        //and many more stations
        
        Game.addStation(station: passwords)
        Game.addStation(station: socialmedia)
        print("Game has been resetted.")
    }
    
    /**
     Function sets up the station Passwords with five tasks as we have a cube:
     First, the station gets an internal name "Station Passwords", and it is set to be not solved yet.
     Second, the tasks are added. Every task gets a name an a description and is set to be not solved yet.
     Afterwards every task is added to the station.
     - Returns: Function returns the final station as an object of Station.
     */
    private static func _prepareStationPasswords () -> Station{
        let passwords = Station()
        passwords.setName(name: "Station Passwords")
        passwords.setSolved(solved: false)
        
        let task1 = Task()
        task1.setName(name: "Brute Force Attack")
        task1.setDescription(description: "Ein leichtes und ein schwereres Passwort knacken.")
        task1.setSolved(solved: false)
        passwords.addTask(task: task1)
        
        let task2 = Task()
        task2.setName(name: "Ranking Passwords")
        task2.setDescription(description: "Verschieden komplexe Passwort nach ihrer Sicherheit in eine Reihenfolge bringen.")
        task2.setSolved(solved: false)
        passwords.addTask(task: task2)
        
        let task3 = Task()
        task3.setName(name: "Types of Usage")
        task3.setDescription(description: "Nutzungsarten: Gemeinsam genutztes Passwort, persönliches Passwort, Einmal-Passwort")
        task3.setSolved(solved: false)
        passwords.addTask(task: task3)
        
        let task4 = Task()
        task4.setName(name: "Good Idea - Bad Idea")
        task4.setDescription(description: "gute und schlechte Aufbewahrungsmöglichkeiten von Passwörtern - Post-it unter der Tastatur, als Notiz im Handy, als Klartextdatei auf dem Computer, Passwortmanager")
        task4.setSolved(solved: false)
        passwords.addTask(task: task4)
        
        let task5 = Task()
        task5.setName(name: "Create a Safe Password")
        task5.setDescription(description: "Ein sicheres Passwort erstellen. Rekapituliere das bisher Gelernte und nutze es. Erhalte ein entsprechendes Collectable.")
        task5.setSolved(solved: false)
        task5.setFinal(isFinal: true)
        passwords.addTask(task: task5)
        
        return passwords
    }
    /**
     Function sets up the station Social Media with five tasks as we have a cube:
     First, the station gets an internal name "Station Social Media", and it is set to be not solved yet.
     Second, the tasks are added. Every task gets a name an a description and is set to be not solved yet.
     Afterwards every task is added to the station.
     IMPORTANT: In the actual state of the game this station is never used. There is no artifact in the real world to be scanned. So this is more of an example how to set up new stations. You should give every task a specific name and description.
     - Returns: Function returns the final station as an object of Station.
     */
    private static func _prepareStationSocialmedia () -> Station{
        let passwords = Station()
        passwords.setName(name: "Station Social Media")
        passwords.setSolved(solved: false)
        
        let task1 = Task()
        passwords.addTask(task: task1)
        
        let task2 = Task()
        passwords.addTask(task: task2)
        
        let task3 = Task()
        passwords.addTask(task: task3)
        
        let task4 = Task()
        passwords.addTask(task: task4)
        
        let task5 = Task()
        passwords.addTask(task: task5)
        
        let task6 = Task()
        task6.setFinal(isFinal: true)
        passwords.addTask(task: task6)
        
        return passwords
    }
    
}
