//
//  Station.swift
//  Lets Literate
//
//  Created by Aljoscha Peters on 02.02.22.
//

import Foundation

/**
 Internal representation of a station with different tasks.
 Each station has an external representation as an artifact - like a cube - with faces. The artifact is the station, the faces are the tasks.
 Scanning a station's start face starts also the station in the app.
 */

class Station {
    
    /// Name of the station, initialized as empty string
    private var name : String = ""
    /// Has the station been solved? initialized as false, of course
    private var solved : Bool = false
    /// Array of tasks of the station. As we may have different artifacts with different number of faces, an array makes most sense. Initialized as empty array.
    private var tasks : [Task] = []
    
    /**
     Sets a new name.
     - Parameter name: new name for the station
     */
    public func setName (name:String) {
        self.name = name
    }
    /**
     Returns the name of the station
     - Returns: Returns the actual name
     */
    public func getName () -> String {
        return self.name
    }
    /**
     Sets the status  of the station, solved or not solved.
     - Parameter solved: True, if station is solved; use false otherwise.
     */
    public func setSolved (solved:Bool) {
        self.solved = solved
    }
    /**
     Returns the status if the station has been solved already.
     - Returns: True, if station is solved, false otherwise.
     */
    public func isSolved () -> Bool {
        return self.solved
    }
    /**
     Adds the given task to the internal array of tasks that belong to the station.
     - Parameter task: initialized new task
     */
    public func addTask (task : Task) {
        self.tasks.append(task)
    }
    /**
     Returns the array of tasks.
     - Returns: Array of objects of type Task
     */
    public func getTasks () -> [Task] {
        return self.tasks
    }
    /**
     Returns a list of all solved tasks of the station.
     - Returns: Array of tasks that has been solved already at this station.
     */
    public func getSolvedTasks () -> [Task] {
        var solvedTasks : [Task] = []
        for task in tasks {
            if task.isSolved() == true {
                solvedTasks.append(task)
            }
        }
        return solvedTasks
    }
    ///internal status if all stations has been solved; this should be calculated dynamically in a productive app.
    private static var allStationsSolved = false
    /**
     Sets the internal status that all stations has been solved. In a productive app this should be done dynamically.
     - Parameter solved: True, if all station has been solved. Use false otherwise.
     */
    public static func setAllStationsSolved (solved:Bool) {
        Station.allStationsSolved = solved
    }
    /**
     Returns the value of the internal status variable. In a productive app this should be done dynamically.
     - Returns: True or false.
     */
    public static func getAllStationsSolved () -> Bool {
        return Station.allStationsSolved
    }
}
