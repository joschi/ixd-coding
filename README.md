# Kim - Explore, Collect, Reflect 

An AR-Game for Primary Students about Digital Sovereignty

## ToS
1. Description
2. Installation
3. Usage
4. Acknowledgements
5. Experts
6. License
7. Developers

### 1. Description
Kim turns the classroom into an adventure: Students explore the universe of the Internet in an augmented reality world. Together 
with an avatar, they master the hurdles of their everyday lives. Riddles on relevant topics are solved: How does social media work? 
What happens when I share pictures? How do I protect my mental health? How do I handle passwords correctly? 

Each action cube that is distributed by the teacher in the learning space before the game starts, represents a topic.
As soon as the students find a cube, they scan it and thus they immerse themselves in the respective learning world, which connects the virtual world with the real world. 
After a short introduction, the students rotate the action cubes to small, motivating tasks in the subject area, which they solve together in small groups. 
To do this, they use different building blocks that have a different meaning at each station. Each action cube ends with a more complex task 
that requires prior experience and learning. Once the students complete it, they receive an AR object. Kim contributes to digital education in schools. 
To promote collaborative learning in class, teams compare and discuss the AR objects they have collected with the teacher. The focus is on 
joint reflection rather than on competitive thinking. Students in grades 5 and 6 experience with Kim interactively and exploratively how to make responsible, 
confident, and (self-)conscious decisions in their everyday activities as we live in an increasingly digitalized world. 

### 2. Installation
This is an iOS app. You need to have an iPhone 8 or newer with iOS 15 or newer installed.
For installation you need to have Xcode and macOS Monterey (older versions could work, but were not tested). Open the project in Xcode and upload the project via Xcode to the iPhone. If you run in problems see https://developer.apple.com.

### 3. Usage
You have to go into Settings -> General -> Device Management and trust the shown developer for the app. Afterwards you can start the app from your homescreen.
For a working experience you also need the cube and stones that were designed for the game. The app searches for them to start interaction with the user.

### 4. Acknowledgements
We used the frameworks from Apple to develop the app - Swift, UIKit, ARKit, etc.
For the AR scenes we used Reality Composer by Apple.
We relied on the book raywenderlich Tutorial Team (ed.), Apple Augmented Reality by Tutorials. First Edition, 2020.
For the documentation we used draw.io.
Adobe products were used to create posters and and many other parts of the documentation.

### 5. Experts
We took advantage of some external experts and we thank esp.
- Elizabeth Burrows, Head of Primary School at Phorms Campus Berlin Süd
- Katinka Lotz, Product and Process Designer at die Baupiloten BDA

### 6. License
This project is published unter GNU General Public License v3.0.

### 7. Developers
Milda Benjes (Computer Science, FU Berlin)
Nik Naumov (Computer Science, FU Berlin)
Aljoscha Peters (Computer Science/ History/ Education, FU Berlin)
Alexia von Salomon (Product Design, KHB) www.alexiavonsalomon.de
